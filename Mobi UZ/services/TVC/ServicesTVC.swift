//
//  ServicesTVC.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/5/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

class ServicesTVC: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionLbl.isHidden = true
    }
    
    func setData(name: String, description: String) {
        nameLbl.text = name
        descriptionLbl.text = description
    }
    

}
