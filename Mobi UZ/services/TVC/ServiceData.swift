//
//  ServiceData.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/5/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

struct ServiceData {
    var name: String
    var details: String
    var code: String?
}
