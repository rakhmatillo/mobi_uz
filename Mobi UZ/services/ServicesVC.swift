//
//  ServicesVC.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/5/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit
import CoreData


class ServicesVC: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var serviceTableView: UITableView!
    @IBOutlet weak var noConnectionLbl: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var data: [Services] = []
    var arr: [Bool] = []
    var codes = ["*100#", "*171*1*2#", "*171*1*3#", "*102#", "*150#", "*151#", "*140#"]
    var type = 0
    
    var lang = def.integer(forKey: "LANG")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLbl.text = type == 0 ? K.L_SERVICES[lang] : K.BALANCE_BALANCE[lang]
        noConnectionLbl.text = K.L_NO_CONNECTION_MESSAGE[lang]
        mainView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        mainView.layer.cornerRadius = 32
        
        fetchData()
        serviceTableView.register(UINib(nibName: "ServicesTVC", bundle: nil), forCellReuseIdentifier: "ServicesTVC")
        serviceTableView.register(UINib(nibName: "ActionTVC", bundle: nil), forCellReuseIdentifier: "ActionTVC")
        serviceTableView.delegate = self
        serviceTableView.dataSource = self
        
        if type == 0 && data.isEmpty{
            indicator.startAnimating()
            indicator.isHidden = false
            noConnectionLbl.isHidden = false
        }else{
            indicator.stopAnimating()
            indicator.isHidden = true
            noConnectionLbl.isHidden = true
        }
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        makeArrayToHideServiceDescription()
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func fetchData() {
        let request: NSFetchRequest<Services> = Services.fetchRequest()
        do {
            try data = K.context.fetch(request)
        } catch {
            print("ERROR IN READING TARIFFS DATABASE\(error)")
        }
    }
    
    func makeArrayToHideServiceDescription() {
        for _ in data {
            arr.append(true)
        }
    }
    
}


extension ServicesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return type == 0 ? data.count : 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if type == 0 {
            
            let cell = serviceTableView.dequeueReusableCell(withIdentifier: "ServicesTVC", for: indexPath) as! ServicesTVC
            cell.descriptionLbl.isHidden = arr[indexPath.row]
            let i = indexPath.row
            if lang == 0{
                cell.setData(name: data[i].name_uz!, description: data[i].desc_uz!)
            }else{
                cell.setData(name: data[i].name_ru!, description: data[i].desc_ru!)
            }
            return cell
            
        } else {
            let secondData = [K.BALANCE_BALANCE[lang],
                              K.BALANCE_LAST_PAYMENT[lang],
                              K.BALANCE_MY_SPENDINGS[lang],
                              K.BALANCE_INTERNET_REMAINDER[lang],
                              K.BALANCE_MY_NUMBER[lang],
                              K.BALANCE_ALL_MY_NUMBERS[lang],
                              K.BALANCE_CHECK_ACTIVE_SERVICES[lang],
                              K.CANCEL[lang]]
            
            let cell = serviceTableView.dequeueReusableCell(withIdentifier: "ActionTVC", for: indexPath) as! ActionTVC
            cell.setData(name: secondData[indexPath.row])
            return cell
        }
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? ServicesTVC
        let cell1 = tableView.cellForRow(at: indexPath) as? ActionTVC
        
        if type == 0 {
            if arr[indexPath.row] {
                cell!.descriptionLbl.isHidden = false
                
            } else {
                if indexPath.row == data.count - 1 {
                    guard let url = URL(string: "https://ip.Mobi.uz/selfcare") else { return }
                    UIApplication.shared.open(url)
                }else{
                    makePhoneCall(phoneNumber: data[indexPath.row].code!)
                }
            }
            arr[indexPath.row] = !arr[indexPath.row]
            tableView.reloadRows(at: [indexPath], with: .automatic)
            
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                cell1?.transform = CGAffineTransform(scaleX: 3, y: 3)
            }, completion: nil)
            
            UIView.animate(withDuration: 0.3) {
                cell1?.transform = .identity
            }
            makePhoneCall(phoneNumber: codes[indexPath.row])
        }
        
    }
    
}



//MARK: Gesture Delegate
extension ServicesVC:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

