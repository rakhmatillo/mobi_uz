//
//  NewsVC.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/8/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit
import WebKit
import Network

class NewsVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var noConnectionLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var webView: WKWebView!
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "InternetConnectionMonitor")
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = K.L_NEWS[def.integer(forKey: "LANG")]
        noConnectionLbl.text = K.L_PLEASE_TURN_ON_INTERNET[def.integer(forKey: "LANG")]
        noConnectionLbl.isHidden = false
        mainView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        mainView.layer.cornerRadius = 32
        webView.navigationDelegate = self
        
        webView.load(URLRequest(url: URL(string: "https://mobi.uz/uz/news/")!))
       navigationController?.interactivePopGestureRecognizer?.delegate = self

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        monitor.pathUpdateHandler = { pathUpdateHandler in
            if pathUpdateHandler.status == .satisfied {
                DispatchQueue.main.async {
                    print("satisfied")
                    self.noConnectionLbl.isHidden = true
                    self.webView.load(URLRequest(url: URL(string: "https://mobi.uz/uz/news/")!))
                }
                
            } else {
                DispatchQueue.main.async {
                    print("notsatisfied")
                   
                }
            }
        }
        monitor.start(queue: queue)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

}
//MARK: Gesture Delegate
extension NewsVC:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

