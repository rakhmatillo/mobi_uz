//
//  NewsTVC.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/8/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

class NewsTVC: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setData(title: String, description: String) {
        titleLbl.text = title
        descriptionLbl.text = description
    }
    
}
