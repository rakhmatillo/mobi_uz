//
//  NewsData.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/9/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

struct NewsData {
    var name: String
    var details: String
    var link: String?
}
