//
//  ViewController.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 5/22/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import CoreData
import StoreKit


let def = UserDefaults.standard
let languageNotification = Notification.Name(rawValue: "language.notification")

func makePhoneCall(phoneNumber: String) {
    let phoneURL = NSURL(string: ("tel://" + phoneNumber))
    UIApplication.shared.open(phoneURL! as URL, options: [:], completionHandler: nil)
}

func getCurrentDayAsInteger() -> Int {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd"
    return Int(dateFormatter.string(from: Date()))!
}

func getCurrentMonthAsInteger() -> Int {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MM"
    return Int(dateFormatter.string(from: Date()))!
}


class MainVC: UIViewController {
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var biggestView: UIView!
    @IBOutlet var redViews: [UIView]!
    
    @IBOutlet weak var internetPacketLbl: UILabel!
    @IBOutlet weak var mInuteAndSMSLbl: UILabel!
    @IBOutlet weak var tariffLbl: UILabel!
    @IBOutlet weak var serviceLbl: UILabel!
    @IBOutlet weak var balanceLbl: UILabel!
    @IBOutlet weak var usefulLbl: UILabel!
    
    @IBOutlet weak var noConnectionView: UIView!
    
    @IBOutlet weak var noConnectionTitleLbl: UILabel!
    
    @IBOutlet weak var noConnectionMessageLbl: UILabel!
    
    
    var lang = def.integer(forKey: "LANG")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !UserDefaults.standard.bool(forKey: "FIRST_TIME"){
            noConnectionView.isHidden = false
        }
        updateWhenNeeded()
        navigationController?.setNavigationBarHidden(true, animated:false)
        createObservers()
        postLangNotification()
        Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true) { timer in
          
            if def.bool(forKey: "isSuccess"){
                timer.invalidate()
            }else{
                self.updateWhenNeeded()
            }
        }
       
        
        
    }
    
    
    //Language Notification
    //======= ========= =========== ============ ===========
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateLabels(notification:)), name: languageNotification, object: nil)
    }
    
    func postLangNotification() {
        NotificationCenter.default.post(name: languageNotification, object: nil)
    }
    
    @objc func updateLabels(notification: NSNotification) {
        let lang = def.integer(forKey: "LANG")
        internetPacketLbl.text = K.L_INTERNET_PACKETS[lang]
        mInuteAndSMSLbl.text = K.L_MINUTES_SMS_PACKETS[lang]
        tariffLbl.text = K.L_TARRIFS[lang]
        serviceLbl.text = K.L_SERVICES[lang]
        balanceLbl.text = K.L_BALANCE[lang]
        usefulLbl.text = K.L_HELPFUL[lang]
        noConnectionTitleLbl.text = K.L_NO_CONNECTION_TITLE[lang]
        noConnectionMessageLbl.text = K.L_NO_CONNECTION_MESSAGE[lang]
        
    }
    
    
    //========== ========== ============= =========== ===========
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        mainView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        mainView.layer.cornerRadius = 32
        for i in 0..<redViews.count{
            redViews[i].layer.cornerRadius = redViews[i].frame.width / 4
        }
        
        
        
        
    }
    
    
    
    @IBAction func exitPressed(_ sender: Any) {
        noConnectionView.isHidden = true
    }
    
    
    
    
    //MARK: -OBSERVERS FOR CHANGING LANGUAGE
    
    
    
    func changeLangForMainView(){
        internetPacketLbl.text = K.L_INTERNET_PACKETS[lang]
        mInuteAndSMSLbl.text = K.L_MINUTES_SMS_PACKETS[lang]
        tariffLbl.text = K.L_TARRIFS[lang]
        serviceLbl.text = K.L_SERVICES[lang]
        balanceLbl.text = K.L_BALANCE[lang]
        usefulLbl.text = K.L_HELPFUL[lang]
        
    }
    
    var num = 0
    @IBAction func showMessageToUser(_ sender: Any) {
        num += 1
        if self.num == 5{
            self.showToast(message: "Developers: Rakhmatillo and MukhammadYunus")
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.num = 0
        }
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 30, y: self.view.frame.size.height - 100, width: self.view.frame.width - 60, height: 55))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 2
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    //MARK: - Internet pressed
    @IBAction func internetPacketBtnPressed(_ sender: Any) {
        navigationController?.pushViewController(InternetPackagesVC(nibName: "InternetPackagesVC", bundle: nil), animated: true)
    }
    
    //MARK: - Minute pressed
    @IBAction func minuteAndSmsButtonPressed(_ sender: Any) {
        navigationController?.pushViewController(MinutesPackageVC(nibName: "MinutesPackageVC", bundle: nil), animated: true)
    }
    
    //MARK: - Tariffs pressed
    @IBAction func tarrifsBtnPressed(_ sender: Any) {
        navigationController?.pushViewController(TarrifsVC(nibName: "TarrifsVC", bundle: nil), animated: true)
    }
    
    
    //MARK: - Balance pressed
    @IBAction func balanceBtnPressed(_ sender: Any) {
//        let lang = def.integer(forKey: "LANG")
//        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//
//        let action1 = UIAlertAction(title: K.BALANCE_BALANCE[lang], style: .default, handler: { (_) in makePhoneCall(phoneNumber: "*100#") })
//        let action2 = UIAlertAction(title: K.BALANCE_LAST_PAYMENT[lang], style: .default, handler: { (_) in makePhoneCall(phoneNumber: "*171*1*2#") })
//        let action3 = UIAlertAction(title: K.BALANCE_MY_SPENDINGS[lang], style: .default, handler: { (_) in makePhoneCall(phoneNumber: "*171*1*3#") })
//        let action4 = UIAlertAction(title: K.BALANCE_INTERNET_REMAINDER[lang], style: .default, handler: { (_) in makePhoneCall(phoneNumber: "*102#") })
//        let action5 = UIAlertAction(title: K.BALANCE_MY_NUMBER[lang], style: .default, handler: { (_) in makePhoneCall(phoneNumber: "*150#") })
//        let action6 = UIAlertAction(title: K.BALANCE_ALL_MY_NUMBERS[lang], style: .default, handler: { (_) in makePhoneCall(phoneNumber: "*151#") })
//        let action7 = UIAlertAction(title: K.BALANCE_CHECK_ACTIVE_SERVICES[lang], style: .default, handler: { (_) in makePhoneCall(phoneNumber: "*140#") })
//        let action8 = UIAlertAction(title: K.CANCEL[lang], style: .cancel, handler: nil)
//
//        alert.addAction(action1)
//        alert.addAction(action2)
//        alert.addAction(action3)
//        alert.addAction(action4)
//        alert.addAction(action5)
//        alert.addAction(action6)
//        alert.addAction(action7)
//        alert.addAction(action8)
//
//        self.present(alert, animated: true, completion: nil)
        
        let vc = ServicesVC(nibName: "ServicesVC", bundle: nil)
        vc.type = 1
        navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    //MARK: - Service pressed
    @IBAction func ServicesBtnPressed(_ sender: Any) {
        let vc = ServicesVC(nibName: "ServicesVC", bundle: nil)
        vc.type = 0
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK: - Useful pressed
    @IBAction func usefulBtnPressed(_ sender: Any) {
        
        let lang = def.integer(forKey: "LANG")
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: K.HELPFUL_TELEGRAM_CHANNEL[lang], style: .default, handler: { (_) in
            
            let urlString = "https://t.me/joinchat/AAAAAFhcXW53DB1VN3cTdQ"
            let tgUrl = URL.init(string:urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            if UIApplication.shared.canOpenURL(tgUrl!)
            {
                UIApplication.shared.open(tgUrl!, options: [:], completionHandler: nil)
            }else
            {
                //App not installed.
            }
            
            
            
        })
        
        let action2 = UIAlertAction(title: K.HELPFUL_CALL_US[lang], style: .default, handler: { (_) in
            //CODE TO CALL +998 97 130 09 09
            makePhoneCall(phoneNumber: "+998971300909")
        })
        let action3 = UIAlertAction(title: K.HELPFUL_SHARE[lang], style: .default, handler: { (_) in
            //CODE TO Share App
            
            self.shareApp()
        })
        
        let action4 = UIAlertAction(title: K.L_NEWS[lang], style: .default) { (_) in
            let vc = NewsVC(nibName: "NewsVC", bundle: nil)
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let action5 = UIAlertAction(title: K.HELPFUL_LANGUAGE[lang], style: .default, handler: { (_) in
            
            let alert = UIAlertController(title: nil, message: K.TEXT_CHOOSE_LANG, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "O'zbek tili", style: .default, handler: { (_) in
                def.set(0, forKey: "LANG")
                self.postLangNotification()
            }))
            alert.addAction(UIAlertAction(title: "Русский язык", style: .default, handler: { (_) in
                def.set(1, forKey: "LANG")
                self.postLangNotification()
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        })
        let action6 = UIAlertAction(title: K.HELPFUL_RATE_APP[lang], style: .default, handler: { (_) in
            //CODE TO Rate App
            self.rateApp()
            
        })
        let cancelAction = UIAlertAction(title: K.CANCEL[lang], style: .cancel, handler: nil)
        
        
        
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(action4)
        alert.addAction(action5)
        alert.addAction(action6)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func rateApp() {
        SKStoreReviewController.requestReview()

    }
    
    func updateWhenNeeded() {
        let def = UserDefaults.standard
        let firstTime = def.bool(forKey: "FIRST_TIME")
        let lastDay = def.integer(forKey: "DAY")
        let lastMonth = def.integer(forKey: "MONTH")
        
        
        let update = getCurrentMonthAsInteger() - lastMonth > 0 ? true : getCurrentDayAsInteger() - lastDay < 5 ? false : true
        
        if !firstTime {
            UserDefaults.standard.set(true, forKey: "FIRST_TIME")
            requestToServer()
            
            print("******************FIRST TIME IS WORKING****************")
            
        } else if update {
            requestToServer()
            
            print("******************UPDATE IS WORKING*******************")
        }
    }
    
    func requestToServer() {
        LoadFunctions.updateMinutesDatabase()
        LoadFunctions.updateTariffsDatabase()
        LoadFunctions.updateServicesDatabase()
        LoadFunctions.updateMegabytesDatabase()
    }
    
    
    func shareApp()->Void{
        
        let someText:String = K.TEXT_TO_SHARE[def.integer(forKey: "LANG")]
        let objectsToShare:URL = URL(string: "https://apps.apple.com/app/apple-store/id" + "\(K.appId)")!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.postToTwitter,UIActivity.ActivityType.mail]
        
        self.present(activityViewController, animated: true)
    }
    
}
