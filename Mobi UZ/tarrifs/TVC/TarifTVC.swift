//
//  TarifTVC.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/4/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

protocol ChangeTarifProtocol {
    func changePressed(buttonName: String)
}

  

class TarifTVC: UITableViewCell {

    @IBOutlet weak var tarifNameLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var minutesLbl: UILabel!
    @IBOutlet weak var smsLbl: UILabel!
    @IBOutlet weak var internetLbl: UILabel!
    @IBOutlet weak var changeTarifBtn: UIButton!
    
    var myDelegate: ChangeTarifProtocol?
    
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var name4: UILabel!
    
    var code = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        let i = def.integer(forKey: "LANG")
        changeTarifBtn.setTitle(K.B_ACTIVATE[def.integer(forKey: "LANG")], for: .normal)
        if getScreenSize() == 0{
            changeTarifBtn.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        }else if getScreenSize() == 1{
            changeTarifBtn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        }else{
            changeTarifBtn.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
            
        }
        
        name1.text = K.L_MONTHLY_PAYMENT[i]
        name2.text = K.L_MINUTES_LIMIT[i]
        name3.text = K.L_SMS[i]
        name4.text = K.L_TRAFFIC_LIMIT[i]
                
    }
    func getScreenSize() -> Int{
           if UIScreen.main.bounds.height < 600{
               return 0
           }else if UIScreen.main.bounds.height < 750{
               return 1
           } else{
               return 2
           }
           
           
       }
    
    func setTarifData(tarifName: String, paymentAmount: String, minutes: String, sms: String, internet: String, code: String){
        tarifNameLbl.text = tarifName
        paymentLbl.text = paymentAmount
        minutesLbl.text = minutes
        smsLbl.text = sms
        internetLbl.text = internet
        self.code = code
    }
    
    
    @IBAction func changeTarifBtnPressed(_ sender: UIButton) {
        myDelegate?.changePressed(buttonName: code)
    }
    
    
}
