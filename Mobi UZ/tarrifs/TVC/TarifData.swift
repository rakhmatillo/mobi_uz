//
//  TarifData.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/4/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

struct TarifData {
    var name: String
    var payment: String
    var minute: String
    var sms: String
    var megabyte: String
    var code: String?
}
