//
//  TarrifsVC.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/4/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit
import CoreData

class TarrifsVC: UIViewController, ChangeTarifProtocol{
    

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tarifsTableView: UITableView!
    @IBOutlet weak var noConnectionLbl: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var tarifData: [TariffPackets] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = K.L_TARRIFS[def.integer(forKey: "LANG")]
        noConnectionLbl.text = K.L_NO_CONNECTION_MESSAGE[def.integer(forKey: "LANG")]

        mainView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        mainView.layer.cornerRadius = 32
        
        fetchData()
        tarifsTableView.register(UINib(nibName: "TarifTVC", bundle: nil), forCellReuseIdentifier: "TarifTVC")
        tarifsTableView.dataSource = self
        tarifsTableView.delegate = self
        
        if tarifData.isEmpty{
            indicator.startAnimating()
            indicator.isHidden = false
            noConnectionLbl.isHidden = false
        }else{
            indicator.stopAnimating()
            indicator.isHidden = true
            noConnectionLbl.isHidden = true
        }
        navigationController?.interactivePopGestureRecognizer?.delegate = self
}
    
    @IBAction func backBtnPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func makePhoneCall(phoneNumber: String) {
        let phoneURL = NSURL(string: ("tel://" + phoneNumber))
        UIApplication.shared.open(phoneURL! as URL, options: [:], completionHandler: nil)
    }
    
    func changePressed(buttonName: String) {
        makePhoneCall(phoneNumber: "*171*\(buttonName)*010100180*1#")
    }
   
    func fetchData() {
        let request: NSFetchRequest<TariffPackets> = TariffPackets.fetchRequest()
        do {
            try tarifData = K.context.fetch(request)
        } catch {
            print("ERROR IN READING TARIFFS DATABASE\(error)")
        }
    }
}





   //MARK: - EXTENSIONS
extension TarrifsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tarifData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tarifsTableView.dequeueReusableCell(withIdentifier: "TarifTVC", for: indexPath) as? TarifTVC else { return UITableViewCell()}
        let i = indexPath.row
        var name = ""
        let lang = def.integer(forKey: "LANG")
        if def.integer(forKey: "LANG") == 0{
            name = tarifData[i].name_uz!
            
        }else{
             name = tarifData[i].name_ru!
        }
        cell.setTarifData(tarifName: name, paymentAmount: tarifData[i].price! + " \(K.CURRENCY[lang])", minutes: tarifData[i].mins! + " \(K.TARIFFS_MINUTE[lang])", sms: tarifData[i].sms! + " \(K.L_SMS[lang])", internet: tarifData[i].mb! + " \(K.TARIFFS_MB[lang])", code: tarifData[i].code!)
            
        cell.myDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
               UIView.animate(
                   withDuration: 0.2,
                   delay: 0.02 * Double(indexPath.row),
                   animations: {
                       cell.transform = CGAffineTransform.identity
               })
    }
}

//MARK: Gesture Delegate
extension TarrifsVC:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
