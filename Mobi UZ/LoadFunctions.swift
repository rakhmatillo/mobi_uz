//
//  LoadFunctions.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 6/11/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class LoadFunctions {
   
    
    public static func updateMinutesDatabase() {
        
        Alamofire.request("https://umsdealer.000webhostapp.com/api/minutes", method: .get).responseJSON { (response) in
            switch response.result {
            case .success(_):
                
                var minutesData: [MinuteSMSPackets] = []
                let request: NSFetchRequest<MinuteSMSPackets> = MinuteSMSPackets.fetchRequest()
                do {
                    try minutesData = K.context.fetch(request)
                } catch {
                    print("ERROR IN READING MINUTE AND SMS DATABASE")
                }
                for i in minutesData {
                    K.context.delete(i)
                }
                
                
                let data = response.data!
                let jsonData: JSON = JSON(data)
                for i in 0 ..< jsonData["data"].count {
                    self.loadMinutes(code: jsonData["data"][i]["code"].stringValue,
                                     name: jsonData["data"][i]["name"].stringValue,
                                     price: jsonData["data"][i]["price"].stringValue,
                                     type: jsonData["data"][i]["type"].int64Value)
                }
               
                
            case .failure(_):
                print("FAILURE")
                
            }
        }
        
    }
    
    
    private static func loadMinutes(code: String, name: String, price: String, type: Int64) {
        let data = MinuteSMSPackets(context: K.context)
        data.code = code
        data.name = name
        data.price = price
        data.type = type
        
        do {
            try K.context.save()
        } catch {
            print("ERROR IN WRITING TO MINUTES \(error)")
        }
    }
    
    
    //MARK: -MB
    
    public static func updateMegabytesDatabase() {
        
        Alamofire.request("https://umsdealer.000webhostapp.com/api/packets", method: .get).responseJSON { (response) in
            print("MB RESPONSE")
            switch response.result {
            case .success(_):
                UserDefaults.standard.set(getCurrentDayAsInteger(), forKey: "DAY")
                UserDefaults.standard.set(getCurrentMonthAsInteger(), forKey: "MONTH")
                print(getCurrentDayAsInteger())
                print(getCurrentMonthAsInteger())
                
                print("MB SUCCESS")
                def.set(true, forKey: "isSuccess")
                var internetData: [InternetPackets] = []
                let request: NSFetchRequest<InternetPackets> = InternetPackets.fetchRequest()
                do {
                    try internetData = K.context.fetch(request)
                } catch {
                    print("ERROR IN READING TARIFFS DATABASE")
                }
                for i in internetData {
                    K.context.delete(i)
                }
                
                
                let data = response.data!
                let jsonData: JSON = JSON(data)
                for i in 0 ..< jsonData["data"].count {
                    self.loadInternetPackets(code: jsonData["data"][i]["code"].stringValue,
                                     name: jsonData["data"][i]["name"].stringValue,
                                     price: jsonData["data"][i]["price"].stringValue,
                                     type: jsonData["data"][i]["type"].int64Value)
                }
                
                
                
            case .failure(_):
                print("MB FAILURE")
               def.set(false, forKey: "isSuccess")
                
            }
        }
        
    }
    
    
    
    private static func loadInternetPackets(code: String, name: String, price: String, type: Int64) {
        let data = InternetPackets(context: K.context)
        data.code = code
        data.name = name
        data.price = price
        data.type = type
        
        do {
            try K.context.save()
        } catch {
            print("ERROR IN WRITING TO TARIFFS\(error)")
        }
    }
    
    
    
    
    
    
    //MARK: - TARRIF
    
    public static func updateTariffsDatabase() {
        Alamofire.request("https://umsdealer.000webhostapp.com/api/tariffs", method: .get).responseJSON { (response) in
            print("TARRIF RESPONSE")
            switch response.result {
            case .success(_):
                print("TARRIF SUCCESS")
                var tarifData: [TariffPackets] = []
                let request: NSFetchRequest<TariffPackets> = TariffPackets.fetchRequest()
                do {
                    try tarifData = K.context.fetch(request)
                } catch {
                    print("ERROR IN READING TARIFFS DATABASE")
                }
                for i in tarifData {
                    K.context.delete(i)
                }
                
                
                let data = response.data!
                let jsonData: JSON = JSON(data)
                for i in 0 ..< jsonData["data"].count {
                    self.loadTariffs(code: jsonData["data"][i]["code"].stringValue,
                                     packet: jsonData["data"][i]["packet"].stringValue,
                                     minutes: jsonData["data"][i]["minutes"].stringValue ,
                                     name_ru: jsonData["data"][i]["name_ru"].stringValue,
                                     name: jsonData["data"][i]["name"].stringValue,
                                     price: jsonData["data"][i]["price"].stringValue ,
                                     sms: jsonData["data"][i]["sms"].stringValue)
                }
                
                
            case .failure(_):
                print("TARIFF FAILURE")
                
            }
        }
        
    }
    
    
    
    private static func loadTariffs(code: String, packet: String, minutes: String, name_ru: String, name: String, price: String, sms: String) {
        let data = TariffPackets(context: K.context)
        data.code = code
        data.mb = packet
        data.mins = minutes
        data.name_ru = name_ru
        data.name_uz = name
        data.price = price
        data.sms = sms
        
        do {
            try K.context.save()
        } catch {
            print("ERROR IN WRITING TO TARIFFS\(error)")
        }
    }
    
    
    //MARK: - SERVICES
    
    public static func updateServicesDatabase() {
        
        Alamofire.request("https://umsdealer.000webhostapp.com/api/services", method: .get).responseJSON { (response) in
            switch response.result {
            case .success(_):
                print("SUCCESS in SERVICES")
                var servicesData: [Services] = []
                let request: NSFetchRequest<Services> = Services.fetchRequest()
                do {
                    try servicesData = K.context.fetch(request)
                } catch {
                    print("ERROR IN writing SERVICES DATABASE")
                }
                for i in servicesData {
                    K.context.delete(i)
                }
                
                
                
                
                let data = response.data!
                let jsonData: JSON = JSON(data)
                for i in 0 ..< jsonData["data"].count {
                    self.loadServices(code: jsonData["data"][i]["code"].stringValue,
                                      def_ru: jsonData["data"][i]["def_ru"].stringValue,
                                      def: jsonData["data"][i]["def"].stringValue,
                                      name_ru: jsonData["data"][i]["name_ru"].stringValue,
                                      name: jsonData["data"][i]["name"].stringValue)
                }
                print("\(jsonData["data"][2]["name_ru"].stringValue)")
                
                
            case .failure(_):
                print("FAILURE services")
                
            }
        }
        
    }
    
    private static func loadServices(code: String, def_ru: String, def: String, name_ru: String, name: String) {
        let data = Services(context: K.context)
        data.code = code
        data.desc_ru = def_ru
        data.desc_uz = def
        data.name_ru = name_ru
        data.name_uz = name
        
        do {
            try K.context.save()
        } catch {
            print("ERROR IN WRITING TO SERVICES\(error)")
        }
    }
    
}
