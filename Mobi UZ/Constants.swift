//
//  Constants.swift
//  Mobi UZ
//
//  Created by Rakhmatillo Topiboldiev on 6/10/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

struct K {
   
    public static let appId = "3MB6T49BC5"
    public static let TEXT_TO_SHARE = ["Ushbu ilovani do'stlaringiz bilan baham ko'ring.", "Поделитесь с друзьями"]
    public static let TEXT_CHOOSE_LANG = "Tilni tanlang / Выберите язык"
    //MARK: - Buttons
   public static let B_CHECK_REMAINDER = ["Qoldiqni tekshirish", "Проверить баланc"]
    public static let B_ACTIVATE = ["O'tish", "Перейти"]
    
    //MARK: - NO Connection
    public static let L_NO_CONNECTION_TITLE = ["Internet mavjud emas","Отсутствует подключение к интернету"]
    public static let L_NO_CONNECTION_MESSAGE = ["Ma'lumotlarni yuklash uchun internet yoki Wi-Fini yoqing", "Для того что чтобы загрузить данные необходимо включить мобильный интернет или Wi-Fi"]
    public static let L_PLEASE_TURN_ON_INTERNET = ["Ulanish uchun iltimos internet yoki Wi-Fini yoqing", "Для того что чтобы загрузить необходимо включить мобильный интернет или Wi-Fi"]
     //MARK: - Labels
    public static let L_INTERNET_PACKETS = ["Internet", "Интернет"]
    public static let L_MINUTES_SMS_PACKETS = ["Daqiqa/SMS", "Минуты/SMS"]
    public static let L_TARRIFS = ["Tariflar", "Тарифы"]
    public static let L_SERVICES = ["Xizmatlar", "Услуги"]
    public static let L_BALANCE = ["Balans", "Баланс"]
    public static let L_HELPFUL = ["Foydali", "Полезное"]
    public static let L_NEWS = ["Yangiliklar", "Новости"]
    
    //INTERNET PACKETS
    public static let L_MONTHLY_INTERNET_PACKETS = ["Oylik paketlar", "Пакеты"]
    public static let L_NIGHT_INTERNET_PACKETS = ["Tungi paketlar", "Ночные пакеты"]
    public static let L_NIGHT_DRIVE_PACKETS = ["Tungi drive", "Ночной драйв"]
    public static let L_DAILY_INTERNET_PACKETS = ["Kunlik paketlar", "Суточные пакеты"]
    public static let L_WEEKLY_INTERNET_PACKETS = ["Haftalik paketlar", "Неделные пакеты"]
    public static let L_MINUTES = ["Daqiqalar", "Минуты"]
    public static let L_SMS = ["SMS", "SMS"]
    public static let L_MONTHLY_PAYMENT = ["Oylik to'lov", "Месячная оплата"]
    public static let L_MINUTES_LIMIT = ["Chiquvchi daqiqalar", "Исходящие звонки"]
   
    public static let L_TRAFFIC_LIMIT = ["Trafik hajmi", "Трафик интернета"]
    
    public static let L_NIGHT_DRIVE_DESC = ["Tungi drive xizmati, abonentlarga xar kun soat 00:00 dan 08:00 gacha internetdan cheksiz foydalanish imkonini beradi", "Услуга ночной драйв позволяет абонентам неограниченно пользоваться интернетом каждую ночь с 00:00 до 08:00"]
    public static let L_DAILY_DESC = ["Kunlik paketlarni amal qilish muddati 1 kun","Срок действие всех суточных интернет пакетов 1 сутки"]
    
    public static let B_CHECK_QOLDIQ = ["*171*019#", "*203#", "", "*102#", "*105#"]
    
    //TARIFFS
  
    public static let CURRENCY = ["so'm", "сум"]
    public static let TARIFFS_MINUTE = ["daqiqa", "минут"]
    public static let TARIFFS_MB = ["MB", "МБ"]
    
    
    
    
    //MARK: - Actionsheet
    
    public static let BALANCE_BALANCE = ["Balans","Баланс"]
    public static let BALANCE_LAST_PAYMENT = ["Oxirgi to'lov", "Последная оплата"]
    public static let BALANCE_MY_SPENDINGS = ["Mening xarajatlarim", "Мой расход"]
    public static let BALANCE_INTERNET_REMAINDER = ["Internet qoldig'i", "Остаток трафика"]
    public static let BALANCE_MY_NUMBER = ["Mening raqamim", "Мой номер"]
    public static let BALANCE_ALL_MY_NUMBERS = ["Mening barcha raqamlarim", "Все мои номера"]
    public static let BALANCE_CHECK_ACTIVE_SERVICES = ["Yoqilgan xizmatlarni tekshirish", "Проверка активних услуг"]
    
    public static let HELPFUL_TELEGRAM_CHANNEL = ["Telegram", "Телеграм"]
    public static let HELPFUL_CALL_US = ["Biz bilan bog'lanish", "Позвонить"]
    public static let HELPFUL_SHARE = ["Ulashish", "Поделиться"]
    public static let HELPFUL_LANGUAGE = ["Tilni o'zgartirish", "Сменит языка"]
    public static let HELPFUL_RATE_APP = ["Baholash", "Оценить"]
    
    public static let CANCEL = ["Bekor qilish", "Отмена"]
  
  
    public static let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    public static let NEWS_URL = "https://mobi.uz/uz/news/"
}
