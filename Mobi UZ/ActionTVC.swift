//
//  ActionTVC.swift
//  Mobi UZ
//
//  Created by Mukhammadyunus on 8/5/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

class ActionTVC: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    
    func setData(name: String) {
        nameLbl.text = name
    }
    
    
}
