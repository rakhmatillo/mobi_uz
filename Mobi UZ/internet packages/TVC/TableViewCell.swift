//
//  TableViewCell.swift
//  Mobi UZ
//
//  Created by Rakhmatillo Topiboldiev on 6/5/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit
protocol BuyPacketProtocol {
    func buyPressed(code: String)
}

class TableViewCell: UITableViewCell {

    @IBOutlet weak var mbLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var currencyLbl: UILabel!
    var myDelegate: BuyPacketProtocol?
    var code = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setData(mb: String, price: String, currency: String, code: String) {
        mbLbl.text = mb
        priceLbl.text = price
        currencyLbl.text = currency
        self.code = code
    }
    
    
    @IBAction func buyBtnPressed(_ sender: UIButton) {
        myDelegate?.buyPressed(code: code)
    }
    
}
