//
//  InternetPackagesVC.swift
//  Mobi UZ
//
//  Created by Rakhmatillo Topiboldiev on 6/4/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit
import CoreData

class InternetPackagesVC: UIViewController{
    
    
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var viewsInStack: [UIView]!
    @IBOutlet var lines: [UIView]!
    @IBOutlet var packageNameLbls: [UILabel]!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var noConnectionLbl: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var horizontalScrollView: UIScrollView!
    
    
    var sampleData : [InternetPackets] = []
    var data : [[InternetPackets]] = [[], [], [], [], []]
    
    
    var checker = 0
    var lang = def.integer(forKey: "LANG")
    
    var packageNameData = [""]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = K.L_INTERNET_PACKETS[lang]
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        tableView.register(UINib(nibName: "TableViewCell1", bundle: nil), forCellReuseIdentifier: "TableViewCell1")
        
        packageNameData = [
            K.L_MONTHLY_INTERNET_PACKETS[lang], K.L_NIGHT_INTERNET_PACKETS[lang], K.L_NIGHT_DRIVE_PACKETS[lang], K.L_DAILY_INTERNET_PACKETS[lang], K.L_WEEKLY_INTERNET_PACKETS[lang] ]
        noConnectionLbl.text = K.L_NO_CONNECTION_MESSAGE[lang]
        
        fetchData()
        nameRakhmatillo()
        anotherNameRakhmatillo()
        
        //no connection with internet it will show or hide info about turning on network
        if sampleData.isEmpty{
            noConnectionLbl.isHidden = false
            indicator.startAnimating()
            indicator.isHidden = false
        }else{
            noConnectionLbl.isHidden = true
            indicator.isHidden = true
            indicator.stopAnimating()
        }
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        gesture()
    }
    
    func gesture(){
        let leftRecognizer = UISwipeGestureRecognizer(target: self, action:
            #selector(swipeMade(_:)))
        leftRecognizer.direction = .left
        let rightRecognizer = UISwipeGestureRecognizer(target: self, action:
            #selector(swipeMade(_:)))
        rightRecognizer.direction = .right
        self.view.addGestureRecognizer(leftRecognizer)
        self.view.addGestureRecognizer(rightRecognizer)
    }
    @IBAction func swipeMade(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .left {
            if checker < data.count - 1{
                checker += 1
                UIView.animate(withDuration: 0.3) {
                    self.horizontalScrollView.contentOffset.x += self.view.frame.width / 3.5
                    self.hideAndShowBottomLineAndScroll()
                }
               
            }
        }
        if sender.direction == .right {
            if checker > 0{
                checker -= 1
                UIView.animate(withDuration: 0.3) {
                    self.horizontalScrollView.contentOffset.x -= self.view.frame.width / 3.5
                    self.hideAndShowBottomLineAndScroll()
                }
                
            }
        }
    }
    
    func nameRakhmatillo() {
        //hides all views in stack
        for i in 0..<viewsInStack.count {
            viewsInStack[i].isHidden = true
            lines[i].isHidden = true
        }
        
        //shows the views
        for i in 0..<viewsInStack.count {
            packageNameLbls[i].text = packageNameData[i]
            if i == 0{
                lines[0].isHidden = false
            }
            viewsInStack[i].isHidden = false
            
        }
    }
    
    
    
    func anotherNameRakhmatillo() {
        //sets label to check button
        checkButton.setTitle(K.B_CHECK_REMAINDER[lang], for: .normal)
        if getScreenSize() == 0{
            checkButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        }else if getScreenSize() == 1{
            checkButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        }else{
            checkButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        mainView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        mainView.layer.cornerRadius = 32
        
        
    }
    
    
    @IBAction func pressed(_ sender: UIButton) {
        checker = sender.tag
        hideAndShowBottomLineAndScroll()
    }
    
    func hideAndShowBottomLineAndScroll(){
        for i in 0..<packageNameData.count {
            if i == checker{
                lines[i].isHidden = false
            }else{
                lines[i].isHidden = true
            }
        }
        if checker == 2{
            checkButton.isHidden = true
        }else{
            checkButton.isHidden = false
        }
        tableView.reloadData()
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    //checks if it is iphone 11, 8 or 5
    func getScreenSize() -> Int{
        if UIScreen.main.bounds.height < 600{
            for i in 0..<packageNameLbls.count{
                packageNameLbls[i].font = UIFont.systemFont(ofSize: 14, weight: .semibold)
            }
            return 0
        }else if UIScreen.main.bounds.height < 750{
            for i in 0..<packageNameLbls.count{
                packageNameLbls[i].font = UIFont.systemFont(ofSize: 15, weight: .semibold)
            }
            return 1
        } else{
            for i in 0..<packageNameLbls.count{
                packageNameLbls[i].font = UIFont.systemFont(ofSize: 17, weight: .semibold)
            }
            return 2
        }
        
        
    }
    
    @IBAction func checkRemainderPressed(_ sender: UIButton) {
        
        makePhoneCall(phoneNumber: K.B_CHECK_QOLDIQ[checker])
    }
    
    //MARK:- Writing data to database
    
    func fetchData() {
        let request: NSFetchRequest<InternetPackets> = InternetPackets.fetchRequest()
        do {
            try sampleData = K.context.fetch(request)
            for i in 0 ..< sampleData.count {
                data[Int(sampleData[i].type)].append(sampleData[i])
            }
        } catch {
            print("ERROR IN READING INTERNET DATABASE\(error)")
        }
    }
    
}




//MARK:- TableView delegate

extension InternetPackagesVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if checker == 2 || checker == 3{
            return data[checker].count + 1
        }else{
            return data[checker].count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let i = indexPath.row
        
        if i < data[checker].count{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell else {return UITableViewCell()}
            cell.myDelegate = self
            cell.setData(mb: data[checker][i].name! + " \(K.TARIFFS_MB[lang])", price: data[checker][i].price!, currency: K.CURRENCY[lang], code: data[checker][i].code!)
            
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell1", for: indexPath) as? TableViewCell1 else {return UITableViewCell()}
            if checker == 2{
                cell.lbl.text = K.L_NIGHT_DRIVE_DESC[lang]
            }else{
                cell.lbl.text = K.L_DAILY_DESC[lang]
            }
            return cell
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if getScreenSize() == 0{
            return tableView.frame.height / 6
            
        }else if getScreenSize() == 1{
            return tableView.frame.height / 7
        }else{
            return tableView.frame.height / 8
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        cell.alpha = 0
        //        UIView.animate(
        //            withDuration: 0.3,
        //            delay: 0.02 * Double(indexPath.row),
        //            animations: {
        //                cell.alpha = 1
        //        })
        cell.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(
            withDuration: 0.3,
            delay: 0.02 * Double(indexPath.row),
            animations: {
                cell.transform = CGAffineTransform.identity
        })
        
    }
    
    
}

//MARK: - BUY Protocol
extension InternetPackagesVC: BuyPacketProtocol{
    func buyPressed(code: String) {
        makePhoneCall(phoneNumber: "*171*\(code)*010100180*1#")
    }
    
    
}

//MARK: Gesture Delegate
extension InternetPackagesVC:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}


